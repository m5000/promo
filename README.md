# EBAY PROMO #

### PODSTAWY ###
Zarządzanie promocjami (czasami figurującymi pod nazwą markdownów) odbywa się za pomocą Trading API, i korzysta z dwóch metod:

* __SetPromotionalSale__ (tworzy promocję),
* __SetPromotionalSaleListings__ (wrzuca aukcje do wczesniej utworzonej promocji),
* informacie o promocjach można pobrać za pomocą metody __GetPromotionalSaleDetails__.

Promocje mogą przyjmować następujące stany:

* __Scheduled__: Promocja została zaplanowana do wystawienia. W tym stanie do promocji można dodawać nowe aukcje.
* __Processing__: Promocja jest przetwarzana przez eBay. W tym stanie promocji nie można edytować.
* __Active__: Promocja trwa. W tym stanie można jedynie usuwać aukcje z promocji.
* __Inactive__: Promocja dobiegła końca. Normalnie eBay daje 24 na przedłużenie takich promocji, my traktujemy je jak usunięte.
* __Deleted__: Promocja została zakończona.

Istnieje "twardy" limit na ilość zapytań, które można wykonać w ciągu 24 godzin i wynosi on 5000, przy czym należy pamiętać, że do utworzenia pojedynczej promocji są potrzebne przynajmniej 2 zapytania (SetPromotionalSale aby promocję utworzyć, SetPromotionalSaleListings aby dodać do niej aukcje). Dlatego w skrypcie DAILY_CALL_LIMIT (bootsrap.php) najlepiej ustawić na 2000-2500.

Aukcje nowe i takie, na których zmieniana była cena mogą zostać dodane do promocji dopiero po pewnym czasie, który zależy od kraju, w którym zostały wystawione.

### SKRYPTY ###

Działanie aplikacji opiera się na trzech skryptach:

* __queue_requests.php__, który może przyjąć dowolną ilość argumentów. Jeśli argument jest poprawną nazwą usera, to skrypt doda go do kolejki pobierania. Jeśli argument jest niepoprawną nazwą usera np. na skutek literówki, zostanie zignorowany. W przypadku braku argumentów skrypt doda do kolejki wszystkich userów.

* __send_request.php__, który zbiera z pierwszej kolejki userów, dla których pobiera dane o promocjach (odpytuje ebay o aktualna listę), zapisuje je w pliku i wrzuca do drugiej kolejki.
* __parse_response.php__, który zbiera z drugiej kolejki nazwy plików z odpowiedziami eBaya i obrabia zawarte w nich dane.

#### Parsowanie odpowiedzi eBaya ####
W pierwszej kolejności wypada wspomnieć, że pobierane są jedynie dane o promocjach zaplanowanych, przetwarzanych i trwających (Scheduled, Processing i Active). Jeżeli odpowiedź nie zawiera informacji o jakiejś promocji to z założenia jest ona traktowana jako zakończona. Dlatego skrypt oznacza jako usunięte wszystkie promocje, które nie zostały zaktualizowane po kroku pierwszym.

Następnie, jeśli istnieją jakieś promocje przetwarzane, skrypt przerywa działanie.

Jesli skrypt dalej pracuje, to przystępuje do usunięcia z promocji wszystkich aukcji, których aktualny markdown (ten z eBaya) nie zgadza się z założonym (tym w bazie).

Następnie tworzone są nowe promocje, do których dodawane są aukcje.

Zmiany są zliczane i jeśli przynajmniej jedna aukcja została z promocji usunięta, bądź do promocji dodana, użytkownik zostaje ponownie dodany do kolejki pobierania. (w celu sprawdzenia poprawności)

Skrypt przerwie pracę, jeśli:

* eBay zwrócił błąd w odpowiedzi na pobieranie informacji o trwających promocjach,
* w trakcie usuwania/dodawania aukcji zostanie osiągnięty dzienny limit zapytań do eBaya (edycji promocji).

### LINKI ###

* https://developer.ebay.com/devzone/xml/docs/Reference/ebay/SetPromotionalSale.html,
* https://developer.ebay.com/devzone/xml/docs/Reference/ebay/SetPromotionalSaleListings.html,
* https://developer.ebay.com/devzone/xml/docs/Reference/ebay/GetPromotionalSaleDetails.html.
