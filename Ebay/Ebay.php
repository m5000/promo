<?php
namespace Ebay;


abstract class AbstractApi
{
    function __construct ($token)
    {
        $this->authToken = $token;
    }

    function __call ($name, $arguments)
    {
        $url = $this->getUrl();
        $headers = $this->getHeaders($name);
        $data = array_shift($arguments);
        return self::request($url, $headers, $data);
    }

    abstract function getUrl ();
    abstract function getHeaders ($call);

    final static function request ($url, $headers, $data)
    {
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($connection, CURLOPT_POST, TRUE);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($connection, CURLOPT_URL, $url);
        curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($connection, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($connection);
        curl_close($connection);
        return $response;
    }

    protected $authToken;
}


class EbayApi extends AbstractApi
{
    function __construct
    (
        $authToken,
        $siteID,
        $compatibilityLevel,
        $developerID,
        $applicationID,
        $certificateID
    )
    {
        parent::__construct($authToken);
        $this->siteID = $siteID;
        $this->compatibilityLevel = $compatibilityLevel;
        $this->developerID = $developerID;
        $this->applicationID = $applicationID;
        $this->certificateID = $certificateID;
    }

    function __call ($name, $arguments)
    {
        $data = array_shift($arguments);
        if (!in_array($name, self::$FULL_KEYSET_METHODS))
        {
            $data = $this->signRequestData($data);
        }
        return parent::__call($name, [$data]);
    }

    function getUrl()
    {
        return 'https://api.ebay.com/ws/api.dll';
    }

    function getHeaders ($call)
    {
        $headers = [
            'X-EBAY-API-CALL-NAME: ' . $call,
            'X-EBAY-API-SITEID: ' . $this->siteID,
            'X-EBAY-API-COMPATIBILITY-LEVEL: ' . $this->compatibilityLevel,
        ];

        if (in_array($call, self::$FULL_KEYSET_METHODS))
        {
            $headers[] = 'X-EBAY-API-DEV-NAME:' . $this->developerID;
            $headers[] = 'X-EBAY-API-APP-NAME:' . $this->applicationID;
            $headers[] = 'X-EBAY-API-CERT-NAME:' . $this->certificateID;
        }
        return $headers;
    }

    private function signRequestData ($xml)
    {
        $request = new \DOMDocument;
        $request->loadXML($xml);
        $credentials = $request->createElement('RequesterCredentials');
        $token = $request->createElement('eBayAuthToken', $this->authToken);
        $credentials->appendChild($token);
        $request->documentElement->appendChild($credentials);
        return $request->saveXML();
    }

    private $siteID;
    private $compatibilityLevel;
    private $developerID;
    private $applicationID;
    private $certificateID;

    static $FULL_KEYSET_METHODS = ['FetchToken', 'RevokeToken', 'GetTokenStatus', 'GetSessionID'];
}


class FileTransferApi extends AbstractApi
{
    function getUrl()
    {
        return 'https://webservices.ebay.com/BulkDataExchangeService';
    }

    function getHeaders ($call)
    {
        return [
            'X-EBAY-SOA-OPERATION-NAME: ' . $call,
            'X-EBAY-SOA-SECURITY-TOKEN: ' . $this->authToken
        ];
    }
}


class BulkDataExchangeApi extends AbstractApi
{
    function getUrl()
    {
        return 'https://storage.ebay.com/FileTransferService';
    }

    function getHeaders ($call)
    {
        return [
            'X-EBAY-SOA-OPERATION-NAME: ' . $call,
            'X-EBAY-SOA-SECURITY-TOKEN: ' . $this->authToken
        ];
    }
}
