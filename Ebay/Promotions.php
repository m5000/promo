<?php
namespace Ebay\Promotions;

use \DOMDocument;

const ACTION_ADD = 'Add';
const ACTION_UPDATE = 'Update';
const ACTION_DELETE = 'Delete';

const DISCOUNT_TYPE_PERCENTAGE = 'Percentage';
const DISCOUNT_TYPE_PRICE = 'Price';

const STATUS_ACTIVE = 'Active';
const STATUS_DELETED = 'Deleted';
const STATUS_INACTIVE = 'Inactive';
const STATUS_PROCESSING = 'Processing';
const STATUS_SCHEDULED = 'Scheduled';

const TYPE_DISCOUNT = 'PriceDiscountOnly';
const TYPE_FREE_SHIPPING = 'FreeShippingOnly';
const TYPE_DISCOUNT_AND_FREE_SHIPPING = 'PriceDiscountAndFreeShipping';


function GetPromotionalSaleDetailsRequest ($id=NULL, $status=[])
{
    $request = createRequestDocument('GetPromotionalSaleDetailsRequest');
    if($id !== NULL)
    {
        addNode($request->documentElement, 'PromotionalSaleID', $id);
    }
    foreach($status as $stat)
    {
        addNode($request->documentElement, 'PromotionalSaleStatus', $stat);
    }
    return $request->saveXML();
}


function SetPromotionalSaleRequest ($action, $details)
{
    $request = createRequestDocument('SetPromotionalSaleRequest');
    addNode($request->documentElement, 'Action', $action);
    $saleDetails = addNode($request->documentElement, 'PromotionalSaleDetails');
    foreach ($details as $name => $value)
    {
        addNode($saleDetails, $name, $value);
    }

    return $request->saveXML();
}


function SetPromotionalSaleListingsRequest ($action, $id, $items)
{
    $request = createRequestDocument('SetPromotionalSaleListingsRequest');
    addNode($request->documentElement, 'Action', $action);
    addNode($request->documentElement, 'PromotionalSaleID', $id);
    $onSale = addNode($request->documentElement, 'PromotionalSaleItemIDArray');
    foreach ($items as $itemID)
    {
        addNode($onSale, 'ItemID', $itemID);
    }
    return $request->saveXML();
}


function createRequestDocument ($request)
{
    $document = new DOMDocument('1.0', 'UTF-8');
    $root = $document->createElement($request);
    $root->setAttribute('xmlns', 'urn:ebay:apis:eBLBaseComponents');
    $document->appendChild($root);
    addNode($root, 'ErrorLanguage', 'en_GB');
    return $document;
}


function addNode ($root, $name, $value=NULL)
{
    $node = $root->ownerDocument->createElement($name, $value);
    $root->appendChild($node);
    return $node;
}
