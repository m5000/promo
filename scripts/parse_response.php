<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

const DAILY_CALL_LIMIT = 2000;
const MIN_DURATION = 4;
const MAX_DURATION = 13;

const ERR_TOKEN_EXPIRED = 932;
const ERR_CALLS_LIMIT_EXCEEDED = 518;
const ERR_LISTING_LIMIT_EXCEEDED = 219421;

$queue = Bootstrap\getQueue();
$queue->watchOnly(Bootstrap\QUEUE_RESPONSES);
while($job=$queue->reserve())
{
    $data = json_decode($job->getData(), TRUE);
    $queue->delete($job);
    $userID = $data['userID'];
    $siteID = $data['siteID'];
    $timestamp = $data['timestamp'];

    try
    {
        Bootstrap\info("processing $userID");
        $response = simplexml_load_file($data['response']);

        if ($response->Ack == 'Failure')
        {
            throw new Exception((string)$response->Errors->LongMessage);
        }

        $promotionalSales = $response->PromotionalSaleDetails->PromotionalSale;

        foreach($promotionalSales as $promotion)
        {
            updatePromotionAndItems($promotion, $userID, $siteID, $timestamp);
        }
        markOldPromotionsAsDeleted($userID, $timestamp);

        if (processedPromotionsExist($userID))
        {
            Bootstrap\info("processed promotions exist, aborting");
        }
        else
        {
            $session = Bootstrap\getEbaySession($userID, $siteID);

            $mismatched = deleteMismatchedItems($session, $userID, $siteID);
            Bootstrap\info("$mismatched mismatched listings modified");

            $new = addNewPromotions($session, $userID);
            Bootstrap\info("$new promotional sales added");

            if ($mismatched > 0 || $new > 0)
            {
                $job = json_encode(['userID' => $userID]);
                $queue->putInTube(Bootstrap\QUEUE_REQUESTS, $job);
                Bootstrap\info("queueing $userID for reprocessing");
            }
            Bootstrap\info("$userID success");
        }
    }
    catch (Exception $e)
    {
        $reason = $e->getMessage();
        Bootstrap\info("$userID failure: $reason");
    }
}


function updatePromotionAndItems ($promotion, $userID, $siteID, $timestamp)
{
    $db = Bootstrap\getDbHandle();
    $sale = [
        $promotion->PromotionalSaleID,
        $promotion->PromotionalSaleName,
        $promotion->Status,
        $promotion->DiscountType,
        $promotion->DiscountValue,
        $promotion->PromotionalSaleStartTime,
        $promotion->PromotionalSaleEndTime,
        $promotion->PromotionalSaleType,
        $userID,
        $siteID,
        $timestamp
    ];
    $questionmarks = array_map(function(){return '?';}, $sale);
    $salePlaceholder = implode(',', $questionmarks);
    $itemsPlaceholder = [];
    $items = [];
    foreach ($promotion->PromotionalSaleItemIDArray->ItemID as $itemID)
    {
        $items[] = $promotion->PromotionalSaleID;
        $items[] = $itemID;
        $itemsPlaceholder[] = '(?,?)';
    }
    $itemsPlaceholder = implode(',', $itemsPlaceholder);
    if ($itemsPlaceholder == '')
    {
        $itemsPlaceholder = '()';
    }
    $sql =
<<<SQL
    INSERT INTO `ebay_api_calls`.`ebay_promotions`
    VALUES ($salePlaceholder)
    ON DUPLICATE KEY UPDATE
    name=VALUES(name), status=VALUES(status),
    discount_type=VALUES(discount_type), discount_value=VALUES(discount_value),
    start_time=VALUES(start_time), end_time=VALUES(end_time),
    sale_type=VALUES(sale_type), user_name=VALUES(user_name),
    site_id=VALUES(site_id), last_checked_on=VALUES(last_checked_on);

    DELETE FROM `ebay_api_calls`.`ebay_promotion_items` WHERE promo_id = ?;

    INSERT INTO `ebay_api_calls`.`ebay_promotion_items`
    VALUES $itemsPlaceholder;
SQL;
    $stmt = $db->prepare($sql);
    $data = array_merge($sale, [$promotion->PromotionalSaleID], $items);
    $stmt->execute($data);
}


function markOldPromotionsAsDeleted ($userID, $timestamp)
{
    $db = Bootstrap\getDbHandle();
    $sql =
<<<SQL
    UPDATE `ebay_api_calls`.`ebay_promotions`
    SET status = 'Deleted', last_checked_on = ?
    WHERE status IN ('Active', 'Inactive', 'Scheduled', 'Processing')
      AND user_name = ?
      AND last_checked_on < ?
SQL;
    $stmt = $db->prepare($sql);
    $stmt->execute([$timestamp, $userID, $timestamp]);
}


function processedPromotionsExist ($userID)
{
    $db = Bootstrap\getDbHandle();
    $sql =
<<<SQL
    SELECT count(1)
    FROM `ebay_api_calls`.`ebay_promotions`
    WHERE status = 'Processing'
      AND user_name = ?
SQL;
    $stmt = $db->prepare($sql);
    $stmt->execute([$userID]);
    return $stmt->fetch(PDO::FETCH_NUM)[0] > 0;
}


function deleteMismatchedItems ($session, $userId, $siteId)
{
    $db = Bootstrap\getDbHandle();
    $db->query('set session group_concat_max_len = 100000;');
    $sql =
<<<SQL
    SELECT items.promo_id, group_concat(items.item_id)
    FROM `ebay_api_calls`.`ebay_promotion_items` items
    LEFT JOIN `ebay_api_calls`.`ebay_promotions` promos
      ON items.promo_id = promos.id
    LEFT JOIN `ebay_api_calls`.`active_auction` aa
      ON items.item_id = aa.auction_id
    LEFT JOIN `ebay_api_calls`.`markdown_values` mv
      ON aa.sku = mv.sku
      AND aa.country = mv.country
      AND aa.user_id = mv.user_id
    WHERE promos.status in ('Active', 'Scheduled')
      AND promos.user_name = ?
      AND aa.auction_id IS NOT NULL
      AND (mv.markdown != promos.discount_value OR mv.markdown IS null)
    GROUP BY promo_id
SQL;
    $stmt = $db->prepare($sql);
    $stmt->execute([$userId]);
    $itemsToBeRemoved = $stmt->fetchAll(PDO::FETCH_NUM);
    $count = 0;
    foreach ($itemsToBeRemoved as list($saleId, $items))
    {
        if (removeItemsFromSale($session, $saleId, $items, $userId, $siteId))
        {
            ++$count;
        }
    }
    return $count;
}

function removeItemsFromSale ($session, $saleId, $items, $userId, $siteId)
{
    $items = explode(',', $items);
    $request = Ebay\Promotions\SetPromotionalSaleListingsRequest(Ebay\Promotions\ACTION_DELETE, $saleId, $items);
    $response = $session->SetPromotionalSaleListings($request);
    $result = simplexml_load_string($response);

    logEbayCall($userId, $siteId, 'SetPromotionalSaleListings', $request, $response, $result->Ack);

    return $result->Ack != 'Failure';
}


function addNewPromotions ($session, $userId)
{
    $db = Bootstrap\getDbHandle();
    $db->query('set session group_concat_max_len = 100000;');
    $sql =
<<<SQL
    SELECT users.user_login, users.site_id, mv.markdown,
        group_concat(active.auction_id) AS items
    FROM ebay_api_calls.ebay_users users
    JOIN ebay_api_calls.markdown_values mv
      ON mv.user_id = users.user_login
    JOIN ebay_api_calls.active_auction active
      ON mv.user_id = active.user_id
      AND mv.sku = active.sku
      AND mv.country = active.country
      AND date(active.start_date) < (current_date - interval if(users.site_id = 77, 7, 20) day)
    LEFT JOIN ebay_api_calls.ebay_promoitems items
      ON items.item_id = active.auction_id
      AND items.status in ('Active', 'Scheduled')
    LEFT JOIN (
    select SUM(s.`1M`) as `1M`,aa.sku from ebay_api_calls.active_auction aa
    join (select
        item.Anr as auction_id,
        sum(if(date(orderDate) >= current_date -interval 1
        month,item.itemquantity,0)) as `1M`
    from afterbuy_api.SoldItems item
    join afterbuy_api.Orders ord
        on item.orderid = ord.orderid
        and date(orderDate) >= current_date -interval 1 month
        and date(orderDate) < current_date
    group by item.Anr) s on s.auction_id=aa.auction_id
    group by aa.sku
     ) selling on selling.sku=active.sku
    WHERE users.user_login = ?
      AND mv.markdown is not null
      AND items.item_id IS null
      AND mv.price = if(active.originalprice = 0, active.price, active.originalprice)
    GROUP BY concat(mv.markdown,if(selling.`1M` is not null,1,0))
    ORDER BY SUM(selling.`1M`) DESC
SQL;
    $stmt = $db->prepare($sql);
    $stmt->execute([$userId]);
    $promotionalSales = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $count = 0;

    foreach ($promotionalSales as $sale)
    {
        try
        {
            $count += addPromotion($session, $sale);
        }
        catch (EbayException $e)
        {
            Bootstrap\info($e->getMessage());

            if (   $e->getCode() == ERR_TOKEN_EXPIRED
                || $e->getCode() == ERR_CALLS_LIMIT_EXCEEDED
                || $e->getCode() == ERR_LISTING_LIMIT_EXCEEDED)
            {
                break;
            }
        }
    }

    return $count;
}


/**
 * Distribute items among number of sales
 * so their end dates are days with calls available
 *
 * @param Ebay\EbayApi $session
 * @param array $sale
 * @return number
 */
function addPromotion ($session, $sale)
{
    $userId = $sale['user_login'];

    $dates = getEndDates($userId);
    if (empty($dates))
    {
        return 0;
    }

    $siteId = $sale['site_id'];
    $markdown = $sale['markdown'];
    $items = explode(',', $sale['items']);
    $count = 0;

    foreach ($dates as $day)
    {
        /*
         * break loop it there are no items left to add
         */
        if (count($items) == 0)
        {
            break;
        }

        /*
         * get slice of items that does not exceed call limit
         * or all items left, whichever is lower
         */
        $len = min(DAILY_CALL_LIMIT - $day['calls'], count($items));
        $itemsToAdd = array_splice($items, -$len);

        /*
         * put items on sale and update data in database
         */
        $saleId = addPromotionToEbay($session, $userId, $siteId, $markdown, $day['date']);

        waitForEbay($session, $saleId);

        addItemsToPromotion($session, $saleId, $userId, $siteId, $itemsToAdd);

        $request = Ebay\Promotions\GetPromotionalSaleDetailsRequest($saleId);
        $response = $session->GetPromotionalSaleDetails($request);
        $data = simplexml_load_string($response);

        $promotion = $data->PromotionalSaleDetails->PromotionalSale;
        $timestamp = date('Y-m-d H:i:s');

        updatePromotionAndItems($promotion, $userId, $siteId, $timestamp);

        ++$count;
    }

    return $count;
}


/**
 * Get all dates with available calls left
 *
 * @param string $user user id
 *
 * @return array
 */
function getEndDates ($user)
{
    $db = Bootstrap\getDbHandle();

    $sql =
<<<SQL
    SELECT da.daty AS date, count(pr.item_id) as calls
    FROM pawelp.daty da
    LEFT JOIN ebay_api_calls.ebay_promoitems pr
      ON date(pr.end_time) = da.daty
      AND pr.status in ('Active', 'Scheduled')
      AND pr.user_name = :user
    WHERE dayofweek(da.daty) not in (6,7)
      AND da.daty
        BETWEEN (current_date + interval :min_duration day)
        AND (current_date + interval :max_duration day)
    GROUP BY da.daty
    HAVING count(pr.item_id) < :call_limit
    ORDER BY count(pr.item_id)
SQL;

    $params = [
        ":min_duration" => MIN_DURATION,
        ":max_duration" => MAX_DURATION,
        ":call_limit" => DAILY_CALL_LIMIT,
        ':user' => $user,
    ];

    $stmt = $db->prepare($sql);
    $stmt->execute($params);

    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
 * Call Ebay to create new promotional sale
 *
 * @param Ebay\EbayApi $session
 * @param string $userId
 * @param number $siteId
 * @param string $endDate
 *
 * @throws EbayException
 *
 * @return number
 */
function addPromotionToEbay ($session, $userId, $siteId, $markdown, $endDate)
{
    $startTime = strtotime('+10 minutes');
    $startDate = gmdate('Y-m-d\TH:i:s\.\0\0\0\Z', $startTime);

    $endDate .= 'T23:59:59.000Z';

    $action = Ebay\Promotions\ACTION_ADD;
    $details = [
        'PromotionalSaleName' => sprintf('Automat %s', date('Y-m-d H:i:s')),
        'DiscountType' => Ebay\Promotions\DISCOUNT_TYPE_PERCENTAGE,
        'DiscountValue' => $markdown,
        'PromotionalSaleType' => Ebay\Promotions\TYPE_DISCOUNT,
        'PromotionalSaleStartTime' => $startDate,
        'PromotionalSaleEndTime' => $endDate,
    ];
    $request = Ebay\Promotions\SetPromotionalSaleRequest($action, $details);
    $response = $session->SetPromotionalSale($request);
    $result = simplexml_load_string($response);

    logEbayCall($userId, $siteId, 'SetPromotionalSale',
                $request, $response, $result->Ack);

    if ($result->Ack == 'Failure')
    {
        $message = (string)$result->Errors->LongMessage;
        $code = (int)$result->Errors->ErrorCode;
        throw new EbayException($message, $code);
    }

    return (int)$result->PromotionalSaleID;
}


/**
 * Add items to promotional sale
 *
 * @param Ebay\EbayApi $session
 * @param number $saleId
 * @param string $userId
 * @param number $siteId
 * @param array $items
 *
 * @throws EbayException
 *
 * @return boolean
 */
function addItemsToPromotion ($session, $saleId, $userId, $siteId, $items)
{
    $action = Ebay\Promotions\ACTION_ADD;
    $request = Ebay\Promotions\SetPromotionalSaleListingsRequest($action, $saleId, $items);
    $response = $session->SetPromotionalSaleListings($request);
    $result = simplexml_load_string($response);

    logEbayCall($userId, $siteId, 'SetPromotionalSaleListings',
                $request, $response, $result->Ack);

    if($result->Ack == 'Failure')
    {
        $message = (string)$result->Errors->LongMessage;
        $code = (int)$result->Errors->ErrorCode;
        throw new EbayException($message, $code);
    }

    return TRUE;
}


function waitForEbay ($session, $promotionalSaleId)
{
    $request = Ebay\Promotions\GetPromotionalSaleDetailsRequest($promotionalSaleId);
    for ($i=0; $i<30; ++$i)
    {
        $response = $session->GetPromotionalSaleDetails($request);
        $output = simplexml_load_string($response);
        $status = $output->PromotionalSaleDetails->PromotionalSale->Status;
        if ($status == Ebay\Promotions\STATUS_SCHEDULED)
        {
            break;
        }
        sleep(10);
    }
}


function logEbayCall ($user, $site, $call, $request, $response, $result)
{
    $db = Bootstrap\getDbHandle();
    $sql =
<<<SQL
    INSERT INTO `ebay_api_calls`.`ebay_calls_log`
    (user, site, call_name, call_content, raw_response, response_ack, timestamp)
    VALUES
    (:user, :site, :call, :request, :response, :result, now())
SQL;
    $stmt = $db->prepare($sql);
    $stmt->execute([
            ':user' => $user,
            ':site' => $site,
            ':call' => $call,
            ':request' => $request,
            ':response' => $response,
            ':result' => $result,
    ]);
}

class EbayException extends Exception {}
