<?php
namespace Bootstrap;

$path_base = dirname(__DIR__) . DIRECTORY_SEPARATOR;
$path_ebay = $path_base . 'Ebay' . DIRECTORY_SEPARATOR;
$path_vendor = $path_base . 'vendor' . DIRECTORY_SEPARATOR;

require_once $path_ebay . 'Ebay.php';
require_once $path_ebay . 'Promotions.php';
require_once $path_vendor . 'autoload.php';

use \Exception;
use \PDO;
use \Pheanstalk;
use \Ebay;

const QUEUE_REQUESTS = 'promotions_requests';
const QUEUE_RESPONSES = 'promotions_responses';


function getDbHandle ()
{
    $dsn = 'mysql:host=localhost;port=3306;';
    $user = '';
    $pass = '';
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    ];
    try
    {
        return new PDO($dsn, $user, $pass, $options);
    }
    catch (Exception $e)
    {
        throw new Exception("couldn't create db handle");
    }
}


function getQueue ()
{
    $host = 'localhost';
    $port = 11300;
    return new Pheanstalk\Pheanstalk($host, $port);
}


function getEbaySession ($userId, $siteId)
{
    $db = getDbHandle();
    $stmt = $db->prepare(
<<<SQL
    select token
    from `ebay_api_calls`.`dev_tokens_all`
    where user_id = ?
      and app_active = 1
    limit 1
SQL
    );
    $stmt->execute([$userId]);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return new Ebay\EbayApi($result['token'], $siteId, 863, NULL, NULL, NULL);
}


function info ($msg)
{
    echo date('Y-m-d H:i:s'), '--', $msg, PHP_EOL;
}
