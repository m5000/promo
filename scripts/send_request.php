<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

const SQL =
<<<SQL
    SELECT u.site_id, d.token, d.developer_key,
        d.application_key, d.certificate_key
    FROM `ebay_api_calls`.`ebay_users` u
    JOIN `ebay_api_calls`.`dev_tokens_all` d
    ON u.user_login = d.user_id
    WHERE u.user_login = ? AND d.active = 1
    LIMIT 1
SQL;
const STATUS = [
    Ebay\Promotions\STATUS_ACTIVE,
    Ebay\Promotions\STATUS_PROCESSING,
    Ebay\Promotions\STATUS_SCHEDULED,
];

$queue = Bootstrap\getQueue();
if(count($argv) > 1)
{
    $users = array_slice($argv, 1);
    foreach ($users as $userID)
    {
        sendRequestAndQueueJob($userID, $queue);
    }
}
else
{
    $queue->watchOnly(Bootstrap\QUEUE_REQUESTS);
    while($job=$queue->reserve())
    {
        $data = json_decode($job->getData(), TRUE);
        $queue->delete($job);
        $userID = $data['userID'];
        sendRequestAndQueueJob($userID, $queue);
    }
}


function sendRequestAndQueueJob ($userID, $queue)
{
    $db = Bootstrap\getDbHandle();
    $stmt = $db->prepare(SQL);
    $stmt->execute([$userID]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($user === FALSE)
    {
        Bootstrap\info("user $userID does not exist");
        return;
    }

    Bootstrap\info("requesting $userID's promotions from ebay");
    $session = new Ebay\EbayApi
    (
        $user['token'],
        $user['site_id'],
        689,
        $user['developer_key'],
        $user['application_key'],
        $user['certificate_key']
    );
    $request = Ebay\Promotions\GetPromotionalSaleDetailsRequest(NULL, STATUS);
    $response = $session->GetPromotionalSaleDetails($request);
    $filename = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $userID . '.promo';
    file_put_contents($filename, $response);
    $data = [
        'userID' => $userID,
        'siteID' => $user['site_id'],
        'response' => $filename,
        'timestamp' => date('Y-m-d H:i:s')
    ];
    $queue->putInTube(Bootstrap\QUEUE_RESPONSES, json_encode($data));
    Bootstrap\info("queueing $userID for further processing");
}
