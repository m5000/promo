<?php
require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$db = Bootstrap\getDbHandle();
$sql = 'SELECT user_login FROM `ebay_api_calls`.`ebay_users`';
if (count($argv) > 1)
{
    $names = array_slice($argv, 1);
    $f = function(){ return '?'; };
    $placeholder = implode(',', array_map($f, $names));
    $sql = "$sql where user_login in ($placeholder)";
    $stmt = $db->prepare($sql);
    $stmt->execute($names);
}
else
{
    $stmt = $db->query($sql);
}

$queue = Bootstrap\getQueue();
foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $user)
{
    $data = ['userID' => $user['user_login']];
    $queue->putInTube(Bootstrap\QUEUE_REQUESTS, json_encode($data));
    Bootstrap\info("queueing {$user['user_login']}");
}
